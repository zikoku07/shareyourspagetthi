# == Schema Information
#
# Table name: general_settings
#
#  id                             :integer          not null, primary key
#  page_middle_image_file_name    :string(255)
#  page_middle_image_content_type :string(255)
#  page_middle_image_file_size    :integer
#  page_middle_image_updated_at   :datetime
#  page_middle_image_header       :string(255)
#  page_middle_image_sub_header   :string(255)
#  text_under_countries           :string(255)
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#

class Admin::GeneralSetting < ActiveRecord::Base
  DEFAULT_BANNER = ActionController::Base.helpers.asset_path("/assets/cover_photos/header/default.jpg")
  # attr_accessor :community_banner_file_name, :community_banner_file_size, :community_banner_content_type


  has_attached_file :page_middle_image, :default_url => DEFAULT_BANNER

  #validates_attachment_presence :image
  validates_attachment_size :page_middle_image, :less_than => 9.megabytes
  validates_attachment_content_type :page_middle_image,
                                    :content_type => ["image/jpeg", "image/png", "image/gif",
                                                      "image/pjpeg", "image/x-png"] #the two last types are sent by IE.

  # process_in_background :community_banner, :processing_image_url => "/assets/listing_image/processing.png", :priority => 1
  # process_in_background :community_page_image, :processing_image_url => "/assets/listing_image/processing.png", :priority => 1
  # # process_in_background :image, :processing_image_url => "/assets/listing_image/processing.png", :priority => 1
end
