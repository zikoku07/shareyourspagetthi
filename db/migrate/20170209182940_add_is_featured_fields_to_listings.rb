class AddIsFeaturedFieldsToListings < ActiveRecord::Migration
  def change
    add_column :listings, :is_featured, :boolean,default: false
  end
end
