class CreateLandingPageImages < ActiveRecord::Migration
  def change
    create_table :landing_page_images do |t|
      t.string :category_name
      t.attachment :image
      t.integer :position
      t.timestamps null: false
    end
  end
end
