class AddRecipientAccountFieldToPaymentSettings < ActiveRecord::Migration
  def change
    add_column :payment_settings, :recipient_account, :string
  end
end
