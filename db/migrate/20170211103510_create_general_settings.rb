class CreateGeneralSettings < ActiveRecord::Migration
  def change
    create_table :general_settings do |t|
      t.attachment :page_middle_image
      t.string :page_middle_image_header
      t.string :page_middle_image_sub_header
      t.string :text_under_countries
      t.timestamps null: false
    end
  end
end
