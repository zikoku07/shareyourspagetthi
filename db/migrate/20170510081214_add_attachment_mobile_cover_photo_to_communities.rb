class AddAttachmentMobileCoverPhotoToCommunities < ActiveRecord::Migration
  def self.up
    change_table :communities do |t|
      t.attachment :mobile_cover_photo
    end
  end

  def self.down
    remove_attachment :communities, :mobile_cover_photo
  end
end
