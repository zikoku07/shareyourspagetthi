class AddMobileCoverPhotoProcessingFieldToCommunities < ActiveRecord::Migration
  def change
    add_column :communities, :mobile_cover_photo_processing, :boolean
  end
end
